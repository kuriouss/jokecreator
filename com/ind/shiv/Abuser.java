package com.ind.shiv;

import static java.lang.System.in;
import static java.lang.System.out;

import java.util.Scanner;

public class Abuser {

	public static void main(String[] args) {
		
		Joker joker = new Joker();

		parseArguments(args, joker);
		
		out.println("Enter Abusive Word in capital, enter anything else to exit");

		try(Scanner inputScanner = new Scanner(in);) {

			while (inputScanner.hasNext("[A-Z]+")) {
				String abusiveWord = inputScanner.next();
				out.println(joker.tellJoke(abusiveWord));
			}
			out.printf("\n\nexiting program only capital case words allowed but  found %s", inputScanner.next());
			
		}

	}

	private static void parseArguments(String[] args, Joker joker) {
		if(args.length ==2){
			switch(args[0]) {
			case "-h" :
				out.println("This program prints joke \n use option '-h' along with an abusibe word to print joke immediately \n else enter no other option");
				break;
			case "-w":
				if(args[1].matches("[A-Z]+")){
					out.println(joker.tellJoke(args[1]));
				}
				break;
			default:
				out.printf("\n\ninvalid argument option %s", args[0]);
				System.exit(401);
				break;
			
			}
		}
	}


}
