package com.ind.shiv;

import static java.lang.String.join;

public class Joker {
	
	public String tellJoke(String abusiveWord) {
		String alphabets = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z ";
		String cleanedAlphabets = removeCharactersOfAbusiveWordFromAlpahabet(alphabets, abusiveWord);
		return cleanedAlphabets + emptyLines(2) + "Wondering where have the rest  of alphabets ," + emptyLines(1)
				+ "well here they are" + emptyLines(5) + abusiveWord;
	}

	private String emptyLines(Integer numberOfemptyLines) {
		String emptyLines = "";
		for (int i = 0; i < numberOfemptyLines; i++) {
			emptyLines = emptyLines + "\n";
		}
		return emptyLines;
	}

	private String removeCharactersOfAbusiveWordFromAlpahabet(String alphabets, String abusiveWord) {
		return alphabets.replaceAll("[" + join(",", abusiveWord) + "]", "\b");
	}

}
